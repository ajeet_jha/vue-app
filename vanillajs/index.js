console.log("Hello from index.js");
let state = 0;

const cont = document.getElementById("count");
const inc = document.getElementById("inc");
const dec = document.getElementById("dec");

cont.innerText = state;

inc.addEventListener("click", () => {
    cont.innerText = state++;
})

dec.addEventListener("click", () => {
    cont.innerText = state--;
})
